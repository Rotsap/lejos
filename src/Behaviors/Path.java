package Behaviors;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Stack;

import lejos.nxt.Sound;
import lejos.nxt.comm.NXTConnection;

public class Path {
	private Stack<float[]> position;
	private DataOutputStream out;
	
    public Path(NXTConnection con){
    	
    	out = con.openDataOutputStream();
    	
    }
    
    
	
	public void addPosition(float x, float y){
		float[] pos = new float[]{x, y};
		position.addElement(pos);
		try {
			out.writeFloat(x);
			out.writeFloat(y);
			out.flush();
		} catch (IOException e) {
			Sound.beepSequence();
		}
	}
	
	

}
