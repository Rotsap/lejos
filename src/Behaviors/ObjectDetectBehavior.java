package Behaviors;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Stack;

import lejos.nxt.ColorSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.comm.NXTConnection;
import lejos.robotics.RangeReading;
import lejos.robotics.localization.OdometryPoseProvider;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.Pose;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.objectdetection.Feature;
import lejos.robotics.objectdetection.FeatureDetector;
import lejos.robotics.objectdetection.FeatureListener;
import lejos.robotics.objectdetection.RangeFeatureDetector;
import lejos.robotics.subsumption.Behavior;

public class ObjectDetectBehavior  implements Behavior, FeatureListener{

	private boolean featureDetected = false;
    private DifferentialPilot pilot;
    private UltrasonicSensor us;
    private ColorSensor  cd;
    private TouchSensor bumper;
    private OdometryPoseProvider pose;
    private RangeFeatureDetector rfd;
    private Feature detectedFeature;
    private DataOutputStream out;
    private Stack<Waypoint> position;
    
    private int direction = 0 ;
    
    public ObjectDetectBehavior(DifferentialPilot pilot, NXTConnection con){
    	this.pilot = pilot;
    	out = con.openDataOutputStream();
    	pose = new OdometryPoseProvider(pilot);
    	cd = new ColorSensor(SensorPort.S3);
    	us = new UltrasonicSensor(SensorPort.S2);
    	bumper = new TouchSensor (SensorPort.S1);
    	rfd = new RangeFeatureDetector(us, 50, 250);
    	rfd.addListener(this);
    	position = new Stack<>();
    	position.push(new Waypoint(0,0));
    }
	
	

	@Override
	public boolean takeControl() {
		return featureDetected;
	}

	@Override
	public void action() {
		RangeReading range = detectedFeature.getRangeReading();
		position.push(new Waypoint(pose.getPose().getX(), pose.getPose().getY()));
		if(!range.invalidReading()){
			Pose position = pose.getPose();
			us.ping();
			//RConsole.println(position+"");
			try
			{
				Thread.sleep(50);
			} catch (InterruptedException e)
			{}
			
			//int colorId = cd.getColorID();
			//folowBlack(colorId);//this makes it follow a black line
			
			int distance = us.getDistance();//get the distance to objects ahead of the robot
			ultra(distance, position);//if it get to close it will activate the lookaround method
						
			if(bumper.isPressed()){
				Sound.beepSequence();
				backing(40);
				lookAround();
				forward(0);
			}
			
		
		rfd.enableDetection(false);
		Thread.yield();
		}
	}
	
	public void addPosition(Pose p){
		position.push(new Waypoint(p.getX(), p.getY()));
		
		try {
			out.writeFloat(p.getX());
			out.writeFloat(p.getY());
			out.flush();
		} catch (IOException e) {
			Sound.beepSequence();
			System.exit(0);
		}
		
	}
	
	@Override
	public void suppress() {
		
	}
	
	@Override
	public void featureDetected(Feature feature, FeatureDetector detector) {
		detectedFeature= feature;
		featureDetected = true;
		
	}
	
	public void folowBlack(int colorId){
		if(colorId == 7){
			forward(0);
		}else{
			findblack();
		}
			
	}
	
	public void findblack(){
		if(direction == 0){
			for(int i = 1; i < 5; i++){
				pilot.rotate(10);
				if(cd.getColorID()==7){
					return;
				}
				
			}
			pilot.rotate(-50);
			for(int i = 1; i < 5; i++){
					pilot.rotate(-10);
					if(cd.getColorID()==7){
						direction = 1;
						return;
					}
			}
		}else{
			for(int i = 1; i < 5; i++){
				pilot.rotate(-10);
				if(cd.getColorID()==7){
					return;
				}
				
			}
			 pilot.rotate(-50);
			for(int i = 1 
					; i < 5; i++){
				pilot.rotate(
						10);
				if(cd.getColorID()==7){
					direction = 0;
					return;
				}
				
			}
		}
		
		
	}
	
	public void ultra (int distance, Pose pos){
		if(distance < 30){	
			lookAround();
			addPosition(pos);
		}
		forward(0);
	 }
	
	public void lookAround(){
		int left = 0;
		int right = 0;

		pilot.stop();
		Motor.C.rotate(90);
		left = us.getDistance();
		Motor.C.rotate(-180);
		right = us.getDistance();
		Motor.C.rotate(90);
		if(left > right){
			turn(-50);
		}else{
			turn(50);
		}
	}
	
	public void turn(int distance){
		pilot.rotate(distance);
	}
	
	public void forward(int TurnRate){
		pilot.steer(TurnRate);
	}
	
	public void backing(double a){
		pilot.travel(-a);
	}

}
