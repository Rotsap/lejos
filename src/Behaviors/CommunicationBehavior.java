package Behaviors;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import javax.bluetooth.RemoteDevice;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.objectdetection.Feature;
import lejos.robotics.objectdetection.FeatureDetector;
import lejos.robotics.objectdetection.FeatureListener;
import lejos.robotics.subsumption.Behavior;

public class CommunicationBehavior implements Behavior, FeatureListener{
	private DifferentialPilot  pilot;
	private boolean _suppressed = false;
    private static DataOutputStream out;
    private static DataInputStream inn;
    private String name = "NoFun";
	
	public CommunicationBehavior() throws IOException{
		//out = con.openDataOutputStream();
		//inn = con.openDataInputStream();
		
		RemoteDevice btrd = Bluetooth.getKnownDevice(name);
		
		
		if (btrd == null) {
			  LCD.clear();
			  LCD.drawString("No such device", 0, 0);
			  Button.waitForAnyPress();
			  System.exit(1);
			}
		
		
		BTConnection btc = Bluetooth.connect(btrd);
		
		if (btc == null) {
			  LCD.clear();
			  LCD.drawString("Connect fail", 0, 0);
			  Button.waitForAnyPress();
			  System.exit(1);
			}else{
				LCD.drawString("Got connection", 0, 0);
				//out.writeUTF("Hello World");
			}
	}
	
	
	
	@Override
	public boolean takeControl() {
		return true;
	}

	@Override
	public void action() {
		_suppressed = false;
		pilot.forward();
		while (! _suppressed){
			Thread.yield();
		}
		pilot.stop();
	}

	@Override
	public void suppress() {
		_suppressed = true;
		
	}

	@Override
	public void featureDetected(Feature feature, FeatureDetector detector) {
		// TODO Auto-generated method stub
		
	}


   
}
