package Behaviors;

import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.subsumption.Behavior;


public class ExploreBehavior implements Behavior{

	private DifferentialPilot  pilot;
	private boolean _suppressed = false;

	public ExploreBehavior(DifferentialPilot pilot)
    {
	this.pilot = pilot;
    } 
	
	@Override
	public boolean takeControl() {
		return true;
	}

	@Override
	public void action() {
		_suppressed = false;
		pilot.forward();
		while (! _suppressed){
			Thread.yield();
		}
		pilot.stop();
	}

	@Override
	public void suppress() {
		_suppressed = true;
		
	}
	
    
}
