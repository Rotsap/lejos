package Behaviors;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Stack;

import lejos.nxt.ColorSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.comm.NXTConnection;
import lejos.robotics.RangeReading;
import lejos.robotics.localization.OdometryPoseProvider;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.Pose;
import lejos.robotics.objectdetection.Feature;
import lejos.robotics.objectdetection.FeatureDetector;
import lejos.robotics.objectdetection.FeatureListener;
import lejos.robotics.objectdetection.RangeFeatureDetector;
import lejos.robotics.subsumption.Behavior;

public class PathFinder implements Behavior, FeatureListener{

	private boolean suppressed = false;
    private boolean featureDetected = false;
    private Feature detectedFeature;
    
    private DifferentialPilot pilot;
    private DataOutputStream out;
    private OdometryPoseProvider pose;
    
    private UltrasonicSensor us;
    private RangeFeatureDetector rfd;
	
    private int count = 0;
    private float[] xx;
    private float[] yy;
    private boolean start = true;
    private Stack position;

    public PathFinder(DifferentialPilot pilot, NXTConnection con){
    	this.pilot = pilot;
    	out = con.openDataOutputStream();
    	pose = new OdometryPoseProvider(pilot);
    	
    }
    
    
	@Override
	public void action() {
		 
		position.push(pose.getPose());
	}
	

	@Override
	public void suppress() {
		suppressed = true;
		
	}

	@Override
	public void featureDetected(Feature feature, FeatureDetector detector) {
		detectedFeature= feature;
		featureDetected = true;
		
	}

	@Override
	public boolean takeControl() {
		return featureDetected;
	}

}
