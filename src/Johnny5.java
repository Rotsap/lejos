

import java.io.DataInputStream;
import java.io.DataOutputStream;

import javax.bluetooth.RemoteDevice;

import Behaviors.CommunicationBehavior;
import Behaviors.ExitBehavior;
import Behaviors.ExploreBehavior;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;
	

public class Johnny5{
	
	static double diam = DifferentialPilot.WHEEL_SIZE_NXT2;
	static double width = 13.7;
	static DifferentialPilot pilot = new DifferentialPilot(diam, width, Motor.A, Motor.B, false);
    private static DataOutputStream out;
    private static DataInputStream inn;

    private static String name = "NoFun";
    
    
    
    
	public static void main(String [] args) throws Exception {
		
		//NXTConnection con = Bluetooth.waitForConnection();
		
		RemoteDevice btrd = Bluetooth.getKnownDevice(name);
		
		
		if (btrd == null) {
			  LCD.clear();
			  LCD.drawString("No such device", 0, 0);
			  Button.waitForAnyPress();
			  System.exit(1);
			}
		
		
		BTConnection btc = Bluetooth.connect(btrd);
		if (btc == null) {
			  LCD.clear();
			  LCD.drawString("Connect fail", 0, 0);
			  Button.waitForAnyPress();
			  System.exit(1);
			}else{
				LCD.drawString("Got connection", 0, 0);

				DataOutputStream dos = btc.openDataOutputStream();
				//out.writeUTF("Hello World");
				//out.writeChars("Hello World");
				//out.flush();
				String mes = "hello world";
				//btc.write(mes.getBytes(), mes.length());
				dos.writeInt(121);
				dos.flush();
				Button.waitForAnyPress();
			}
    	
		
		//RConsole.openAny(0);
		/*pilot.setTravelSpeed(8);
		
		Behavior b1 = new ExploreBehavior(pilot);

		Behavior b2 = new CommunicationBehavior();
		//Behavior b2 = new PathFinder(pilot, con);
		//Behavior b2 = new ObjectDetectBehavior(pilot, con);
		
		Behavior b3 = new ExitBehavior();
		
		Behavior[] behaviorList = { b1, b2, b3 };
		Arbitrator arbitrator = new Arbitrator(behaviorList);
		
		//Button.waitForAnyPress();
		
		arbitrator.start();*/
			
	}	
	
}
